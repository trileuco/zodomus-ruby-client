# ZodomusClient::PropertyAreaInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attractions** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyAreaInfo.new(attractions: 1)
```


