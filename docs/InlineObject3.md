# ZodomusClient::InlineObject3

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | 
**property_id** | **String** |  | 
**status** | **String** |  | 
**reservation_id** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject3.new(channel_id: 1,
                                 property_id: 999999,
                                 status: new,
                                 reservation_id: 111211212)
```


