# ZodomusClient::PropertyTpaExtensions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sell_meals_through_booking** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyTpaExtensions.new(sell_meals_through_booking: 1)
```


