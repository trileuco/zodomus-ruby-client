# ZodomusClient::PropertyContactInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**addresses** | [**PropertyContactInfoAddresses**](PropertyContactInfoAddresses.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyContactInfo.new(type: PhysicalLocation,
                                 addresses: null)
```


