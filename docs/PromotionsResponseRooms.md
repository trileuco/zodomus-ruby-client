# ZodomusClient::PromotionsResponseRooms

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**room** | [**Array&lt;PromotionsResponseRoomsRoom&gt;**](PromotionsResponseRoomsRoom.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseRooms.new(room: null)
```


