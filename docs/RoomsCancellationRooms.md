# ZodomusClient::RoomsCancellationRooms

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**room_id** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::RoomsCancellationRooms.new(room_id: 90001)
```


