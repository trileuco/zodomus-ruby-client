# ZodomusClient::PromotionsResponseStayDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsBookDate**](PromotionsBookDate.md) |  | [optional] 
**active_weekdays** | [**PromotionsResponseStayDateActiveWeekdays**](PromotionsResponseStayDateActiveWeekdays.md) |  | [optional] 
**excluded_dates** | [**PromotionsResponseStayDateExcludedDates**](PromotionsResponseStayDateExcludedDates.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseStayDate.new(attributes: null,
                                 active_weekdays: null,
                                 excluded_dates: null)
```


