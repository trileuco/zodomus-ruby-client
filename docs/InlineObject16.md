# ZodomusClient::InlineObject16

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**property_id** | **String** |  | [optional] 
**action** | **String** |  | [optional] 
**reviews_id** | **String** |  | [optional] 
**reply** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject16.new(channel_id: 1,
                                 property_id: 999999,
                                 action: ENABLE or DISMISS,
                                 reviews_id: AD5252522,
                                 reply: Your reply test)
```


