# ZodomusClient::PropertyContactInfoAddresses

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_line** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyContactInfoAddresses.new(address_line: Test street 1)
```


