# ZodomusClient::Room

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**rates** | [**Array&lt;RoomRates&gt;**](RoomRates.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::Room.new(id: null,
                                 name: null,
                                 rates: null)
```


