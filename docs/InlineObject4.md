# ZodomusClient::InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | 
**property_id** | **String** |  | 
**price_model_id** | **Integer** |  | 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject4.new(channel_id: 1,
                                 property_id: 999999,
                                 price_model_id: 1)
```


