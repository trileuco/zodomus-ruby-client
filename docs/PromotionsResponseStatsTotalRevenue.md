# ZodomusClient::PromotionsResponseStatsTotalRevenue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsResponseStatsTotalRevenueAttributes**](PromotionsResponseStatsTotalRevenueAttributes.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseStatsTotalRevenue.new(attributes: null)
```


