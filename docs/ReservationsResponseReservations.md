# ZodomusClient::ReservationsResponseReservations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservation** | [**Reservation**](Reservation.md) |  | [optional] 
**customer** | [**Customer**](Customer.md) |  | [optional] 
**rooms** | [**Array&lt;RoomR&gt;**](RoomR.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsResponseReservations.new(reservation: null,
                                 customer: null,
                                 rooms: null)
```


