# ZodomusClient::PropertyHotelInfoServices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyHotelInfoServices.new(code: 1)
```


