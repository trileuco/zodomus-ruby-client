# ZodomusClient::PromotionsResponseAdditionalDates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional_dates** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseAdditionalDates.new(additional_dates: null)
```


