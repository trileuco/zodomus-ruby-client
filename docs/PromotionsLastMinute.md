# ZodomusClient::PromotionsLastMinute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unit** | **String** |  | [optional] 
**value** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsLastMinute.new(unit: null,
                                 value: null)
```


