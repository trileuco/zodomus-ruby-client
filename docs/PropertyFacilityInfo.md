# ZodomusClient::PropertyFacilityInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guest_rooms** | **String** |  | [optional] 
**restaurants** | **String** |  | [optional] 
**tpa_extensions** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyFacilityInfo.new(guest_rooms: 1,
                                 restaurants: 1,
                                 tpa_extensions: 1)
```


