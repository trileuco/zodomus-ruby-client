# ZodomusClient::ReservationsCCResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**reservation** | [**Array&lt;ReservationsCCResponseReservation&gt;**](ReservationsCCResponseReservation.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsCCResponse.new(status: null,
                                 reservation: null)
```


