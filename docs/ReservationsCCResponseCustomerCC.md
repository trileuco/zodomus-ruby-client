# ZodomusClient::ReservationsCCResponseCustomerCC

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**cvc** | **String** |  | [optional] 
**number** | **String** |  | [optional] 
**date** | **String** |  | [optional] 
**type** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsCCResponseCustomerCC.new(name: null,
                                 cvc: null,
                                 number: null,
                                 date: null,
                                 type: null)
```


