# ZodomusClient::RoomRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**active** | **String** |  | [optional] 
**max_persons** | **Integer** |  | [optional] 
**policy** | **String** |  | [optional] 
**policy_id** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::RoomRates.new(id: null,
                                 name: null,
                                 active: null,
                                 max_persons: null,
                                 policy: null,
                                 policy_id: null)
```


