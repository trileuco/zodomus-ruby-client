# ZodomusClient::InlineObject17

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**property_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**target_channel** | **String** |  | [optional] 
**min_stay_through** | **String** |  | [optional] 
**non_refundable** | **String** |  | [optional] 
**no_cc_promotion** | **String** |  | [optional] 
**last_minute** | [**PromotionsLastMinute**](PromotionsLastMinute.md) |  | [optional] 
**book_date** | [**PromotionsBookDate**](PromotionsBookDate.md) |  | [optional] 
**book_time** | [**PromotionsBookDate**](PromotionsBookDate.md) |  | [optional] 
**stay_date** | [**PromotionsStayDate**](PromotionsStayDate.md) |  | [optional] 
**additional_dates** | [**PromotionsBookDate**](PromotionsBookDate.md) |  | [optional] 
**rooms** | [**Array&lt;PromotionsRooms&gt;**](PromotionsRooms.md) |  | [optional] 
**parent_rates** | [**Array&lt;PromotionsParentRates&gt;**](PromotionsParentRates.md) |  | [optional] 
**discount** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject17.new(channel_id: 1,
                                 property_id: 999999,
                                 name: null,
                                 type: null,
                                 target_channel: null,
                                 min_stay_through: null,
                                 non_refundable: null,
                                 no_cc_promotion: null,
                                 last_minute: null,
                                 book_date: null,
                                 book_time: null,
                                 stay_date: null,
                                 additional_dates: null,
                                 rooms: null,
                                 parent_rates: null,
                                 discount: null)
```


