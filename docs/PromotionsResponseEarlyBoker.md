# ZodomusClient::PromotionsResponseEarlyBoker

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsResponseEarlyBokerAttributes**](PromotionsResponseEarlyBokerAttributes.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseEarlyBoker.new(attributes: null)
```


