# ZodomusClient::OpportunitiesResponseOpportunities

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**opportunity_id** | **String** |  | [optional] 
**cta** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**destination_api** | **String** |  | [optional] 
**implementation** | **String** |  | [optional] 
**title** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::OpportunitiesResponseOpportunities.new(opportunity_id: null,
                                 cta: null,
                                 description: null,
                                 destination_api: null,
                                 implementation: null,
                                 title: null)
```


