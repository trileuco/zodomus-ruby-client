# ZodomusClient::GuestReviewsResponseOpportunities

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_id** | **String** |  | [optional] 
**created_timestamp** | **String** |  | [optional] 
**reservation_id** | **String** |  | [optional] 
**scoring** | [**GuestReviewsResponseScoring**](GuestReviewsResponseScoring.md) |  | [optional] 
**reply** | **String** |  | [optional] 
**reviewer** | [**GuestReviewsResponseReviewer**](GuestReviewsResponseReviewer.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::GuestReviewsResponseOpportunities.new(review_id: null,
                                 created_timestamp: null,
                                 reservation_id: null,
                                 scoring: null,
                                 reply: null,
                                 reviewer: null)
```


