# ZodomusClient::RoomRatesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**rooms** | [**Array&lt;Room&gt;**](Room.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::RoomRatesResponse.new(status: null,
                                 rooms: null)
```


