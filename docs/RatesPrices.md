# ZodomusClient::RatesPrices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price** | **String** |  | [optional] 
**price_single** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::RatesPrices.new(price: ###.##,
                                 price_single: ###.##, not used on single rooms)
```


