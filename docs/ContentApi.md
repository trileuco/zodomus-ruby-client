# ZodomusClient::ContentApi

All URIs are relative to *https://api.zodomus.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**product_post**](ContentApi.md#product_post) | **POST** /product | Create, modify or delete a product (only used in Booking)
[**property_get**](ContentApi.md#property_get) | **GET** /property | Get property details (used only on Expedia)
[**property_post**](ContentApi.md#property_post) | **POST** /property | Create or modify a property (used only in Booking)
[**property_status_post**](ContentApi.md#property_status_post) | **POST** /property-status | Set property status or check property (used only on Booking)
[**rate_get**](ContentApi.md#rate_get) | **GET** /rate | Get rate details (used only on Expedia)
[**rate_post**](ContentApi.md#rate_post) | **POST** /rate | Create, modify or delete a rate
[**room_get**](ContentApi.md#room_get) | **GET** /room | Get room details (used only on Expedia)
[**room_post**](ContentApi.md#room_post) | **POST** /room | Create or modify a room
[**room_status_post**](ContentApi.md#room_status_post) | **POST** /room-status | Set room status (used only on Booking)



## product_post

> product_post(inline_object14)

Create, modify or delete a product (only used in Booking)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
inline_object14 = ZodomusClient::InlineObject14.new # InlineObject14 | 

begin
  #Create, modify or delete a product (only used in Booking)
  api_instance.product_post(inline_object14)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->product_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object14** | [**InlineObject14**](InlineObject14.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## property_get

> property_get(channel_id, property_id)

Get property details (used only on Expedia)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel

begin
  #Get property details (used only on Expedia)
  api_instance.property_get(channel_id, property_id)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->property_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## property_post

> property_post(inline_object9)

Create or modify a property (used only in Booking)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
inline_object9 = ZodomusClient::InlineObject9.new # InlineObject9 | 

begin
  #Create or modify a property (used only in Booking)
  api_instance.property_post(inline_object9)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->property_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object9** | [**InlineObject9**](InlineObject9.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## property_status_post

> property_status_post(inline_object10)

Set property status or check property (used only on Booking)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
inline_object10 = ZodomusClient::InlineObject10.new # InlineObject10 | 

begin
  #Set property status or check property (used only on Booking)
  api_instance.property_status_post(inline_object10)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->property_status_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object10** | [**InlineObject10**](InlineObject10.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## rate_get

> rate_get(channel_id, property_id, room_id, rate_id)

Get rate details (used only on Expedia)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel
room_id = 'room_id_example' # String | Room id supplied by the channel
rate_id = 'rate_id_example' # String | Rate id supplied by the channel

begin
  #Get rate details (used only on Expedia)
  api_instance.rate_get(channel_id, property_id, room_id, rate_id)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->rate_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 
 **room_id** | **String**| Room id supplied by the channel | 
 **rate_id** | **String**| Rate id supplied by the channel | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## rate_post

> rate_post(inline_object13)

Create, modify or delete a rate

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
inline_object13 = ZodomusClient::InlineObject13.new # InlineObject13 | 

begin
  #Create, modify or delete a rate
  api_instance.rate_post(inline_object13)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->rate_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object13** | [**InlineObject13**](InlineObject13.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## room_get

> room_get(channel_id, property_id, room_id)

Get room details (used only on Expedia)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel
room_id = 'room_id_example' # String | Room id supplied by the channel

begin
  #Get room details (used only on Expedia)
  api_instance.room_get(channel_id, property_id, room_id)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->room_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 
 **room_id** | **String**| Room id supplied by the channel | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## room_post

> room_post(inline_object11)

Create or modify a room

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
inline_object11 = ZodomusClient::InlineObject11.new # InlineObject11 | 

begin
  #Create or modify a room
  api_instance.room_post(inline_object11)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->room_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object11** | [**InlineObject11**](InlineObject11.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## room_status_post

> room_status_post(inline_object12)

Set room status (used only on Booking)

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ContentApi.new
inline_object12 = ZodomusClient::InlineObject12.new # InlineObject12 | 

begin
  #Set room status (used only on Booking)
  api_instance.room_status_post(inline_object12)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ContentApi->room_status_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object12** | [**InlineObject12**](InlineObject12.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

