# ZodomusClient::PropertyAffiliationInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**awards** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyAffiliationInfo.new(awards: 1)
```


