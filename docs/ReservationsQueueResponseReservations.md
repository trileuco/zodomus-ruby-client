# ZodomusClient::ReservationsQueueResponseReservations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**date** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsQueueResponseReservations.new(id: null,
                                 status: 1&#x3D;new, 2&#x3D;modified, 3&#x3D;cancelled,
                                 date: null)
```


