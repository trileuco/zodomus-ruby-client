# ZodomusClient::ReservationsCCResponseReservation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**customer_cc** | [**Array&lt;ReservationsCCResponseCustomerCC&gt;**](ReservationsCCResponseCustomerCC.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsCCResponseReservation.new(id: null,
                                 customer_cc: null)
```


