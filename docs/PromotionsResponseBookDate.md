# ZodomusClient::PromotionsResponseBookDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsBookDate**](PromotionsBookDate.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseBookDate.new(attributes: null)
```


