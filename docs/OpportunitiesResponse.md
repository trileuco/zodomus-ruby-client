# ZodomusClient::OpportunitiesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**opportunities** | [**Array&lt;OpportunitiesResponseOpportunities&gt;**](OpportunitiesResponseOpportunities.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::OpportunitiesResponse.new(status: null,
                                 opportunities: null)
```


