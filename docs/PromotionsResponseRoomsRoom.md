# ZodomusClient::PromotionsResponseRoomsRoom

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsResponseRoomsAttributes**](PromotionsResponseRoomsAttributes.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseRoomsRoom.new(attributes: null)
```


