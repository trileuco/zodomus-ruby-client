# ZodomusClient::PropertyMultimediaDescriptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**tag** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyMultimediaDescriptions.new(url: 1,
                                 tag: 1)
```


