# ZodomusClient::ReservationsQueueResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**reservations** | [**Array&lt;ReservationsQueueResponseReservations&gt;**](ReservationsQueueResponseReservations.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsQueueResponse.new(status: null,
                                 reservations: null)
```


