# ZodomusClient::PromotionsStayDateActiveWeekdays

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_weekday** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsStayDateActiveWeekdays.new(active_weekday: null)
```


