# ZodomusClient::PromotionsResponseStayDateExcludedDates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**excluded_date** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseStayDateExcludedDates.new(excluded_date: null)
```


