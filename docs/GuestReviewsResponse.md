# ZodomusClient::GuestReviewsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**opportunities** | [**Array&lt;GuestReviewsResponseOpportunities&gt;**](GuestReviewsResponseOpportunities.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::GuestReviewsResponse.new(status: null,
                                 opportunities: null)
```


