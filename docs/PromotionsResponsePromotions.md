# ZodomusClient::PromotionsResponsePromotions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsResponseAttributes**](PromotionsResponseAttributes.md) |  | [optional] 
**last_minute** | [**PromotionsResponseLastMinute**](PromotionsResponseLastMinute.md) |  | [optional] 
**early_boker** | [**PromotionsResponseEarlyBoker**](PromotionsResponseEarlyBoker.md) |  | [optional] 
**book_date** | [**PromotionsResponseBookDate**](PromotionsResponseBookDate.md) |  | [optional] 
**book_time** | [**PromotionsResponseBookDate**](PromotionsResponseBookDate.md) |  | [optional] 
**stay_date** | [**PromotionsResponseStayDate**](PromotionsResponseStayDate.md) |  | [optional] 
**additional_dates** | [**PromotionsResponseAdditionalDates**](PromotionsResponseAdditionalDates.md) |  | [optional] 
**rooms** | [**PromotionsResponseRooms**](PromotionsResponseRooms.md) |  | [optional] 
**parent_rates** | [**PromotionsResponseParentRates**](PromotionsResponseParentRates.md) |  | [optional] 
**discount** | [**PromotionsResponseRoomsRoom**](PromotionsResponseRoomsRoom.md) |  | [optional] 
**stats** | [**PromotionsResponseStats**](PromotionsResponseStats.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponsePromotions.new(attributes: null,
                                 last_minute: null,
                                 early_boker: null,
                                 book_date: null,
                                 book_time: null,
                                 stay_date: null,
                                 additional_dates: null,
                                 rooms: null,
                                 parent_rates: null,
                                 discount: null,
                                 stats: null)
```


