# ZodomusClient::BookingtablesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**account** | [**Bookingtables**](Bookingtables.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::BookingtablesResponse.new(status: null,
                                 account: null)
```


