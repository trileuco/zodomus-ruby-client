# ZodomusClient::PromotionsResponseParentRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent_rate** | [**PromotionsResponseRoomsRoom**](PromotionsResponseRoomsRoom.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseParentRates.new(parent_rate: null)
```


