# ZodomusClient::AccountResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::AccountResponse.new(status: null,
                                 account: null)
```


