# ZodomusClient::PromotionsResponseStats

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_revenue** | [**PromotionsResponseStatsTotalRevenue**](PromotionsResponseStatsTotalRevenue.md) |  | [optional] 
**nr_room_nights** | **String** |  | [optional] 
**nr_bookings** | **String** |  | [optional] 
**nr_cancellations** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseStats.new(total_revenue: null,
                                 nr_room_nights: null,
                                 nr_bookings: null,
                                 nr_cancellations: null)
```


