# ZodomusClient::ReservationsSummaryResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**reservations** | [**Array&lt;ReservationsSummaryResponseReservations&gt;**](ReservationsSummaryResponseReservations.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsSummaryResponse.new(status: null,
                                 reservations: null)
```


