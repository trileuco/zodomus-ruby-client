# ZodomusClient::RatesAndAvailabilityApi

All URIs are relative to *https://api.zodomus.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**availability_get**](RatesAndAvailabilityApi.md#availability_get) | **GET** /availability | Get the rooms availability for the corresponding channel / property / dates
[**availability_post**](RatesAndAvailabilityApi.md#availability_post) | **POST** /availability | Set your availability in the defined channel / property / room
[**rates_derived_post**](RatesAndAvailabilityApi.md#rates_derived_post) | **POST** /rates-derived | Set rates (derived) for your channel / property / room /rate
[**rates_post**](RatesAndAvailabilityApi.md#rates_post) | **POST** /rates | Set rates for your channel / property / room /rate
[**room_rates_get**](RatesAndAvailabilityApi.md#room_rates_get) | **GET** /room-rates | Get a list of rooms and rates for the corresponding channel / property



## availability_get

> availability_get(channel_id, property_id, date_from, date_to)

Get the rooms availability for the corresponding channel / property / dates

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::RatesAndAvailabilityApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel
date_from = 'date_from_example' # String | Starting date to get availability
date_to = 'date_to_example' # String | Ending date to get availability

begin
  #Get the rooms availability for the corresponding channel / property / dates
  api_instance.availability_get(channel_id, property_id, date_from, date_to)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling RatesAndAvailabilityApi->availability_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 
 **date_from** | **String**| Starting date to get availability | 
 **date_to** | **String**| Ending date to get availability | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## availability_post

> AvailabilityResponse availability_post(inline_object)

Set your availability in the defined channel / property / room

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::RatesAndAvailabilityApi.new
inline_object = ZodomusClient::InlineObject.new # InlineObject | 

begin
  #Set your availability in the defined channel / property / room
  result = api_instance.availability_post(inline_object)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling RatesAndAvailabilityApi->availability_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](InlineObject.md)|  | 

### Return type

[**AvailabilityResponse**](AvailabilityResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rates_derived_post

> RatesResponse rates_derived_post(inline_object2)

Set rates (derived) for your channel / property / room /rate

Used in Booking channel only on derived prices

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::RatesAndAvailabilityApi.new
inline_object2 = ZodomusClient::InlineObject2.new # InlineObject2 | 

begin
  #Set rates (derived) for your channel / property / room /rate
  result = api_instance.rates_derived_post(inline_object2)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling RatesAndAvailabilityApi->rates_derived_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object2** | [**InlineObject2**](InlineObject2.md)|  | 

### Return type

[**RatesResponse**](RatesResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## rates_post

> RatesResponse rates_post(inline_object1)

Set rates for your channel / property / room /rate

Set rates according to the price model defined in the channel property<br> Some parameters depend on the price model. Please check documentation

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::RatesAndAvailabilityApi.new
inline_object1 = ZodomusClient::InlineObject1.new # InlineObject1 | 

begin
  #Set rates for your channel / property / room /rate
  result = api_instance.rates_post(inline_object1)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling RatesAndAvailabilityApi->rates_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object1** | [**InlineObject1**](InlineObject1.md)|  | 

### Return type

[**RatesResponse**](RatesResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## room_rates_get

> RoomRatesResponse room_rates_get(channel_id, property_id)

Get a list of rooms and rates for the corresponding channel / property

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::RatesAndAvailabilityApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel

begin
  #Get a list of rooms and rates for the corresponding channel / property
  result = api_instance.room_rates_get(channel_id, property_id)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling RatesAndAvailabilityApi->room_rates_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 

### Return type

[**RoomRatesResponse**](RoomRatesResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

