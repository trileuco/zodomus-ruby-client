# ZodomusClient::Bookingtables

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**required** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::Bookingtables.new(code: null,
                                 description: null,
                                 value: null,
                                 required: null)
```


