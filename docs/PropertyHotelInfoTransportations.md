# ZodomusClient::PropertyHotelInfoTransportations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**leg** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyHotelInfoTransportations.new(leg: 1)
```


