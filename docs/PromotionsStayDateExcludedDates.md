# ZodomusClient::PromotionsStayDateExcludedDates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**excluded_date** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsStayDateExcludedDates.new(excluded_date: null)
```


