# ZodomusClient::InlineObject9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**hotel_content** | [**PropertyHotelContent**](PropertyHotelContent.md) |  | [optional] 
**contact_info** | [**PropertyContactInfo**](PropertyContactInfo.md) |  | [optional] 
**hotel_info** | [**PropertyHotelInfo**](PropertyHotelInfo.md) |  | [optional] 
**facility_info** | [**PropertyFacilityInfo**](PropertyFacilityInfo.md) |  | [optional] 
**tpa_extensions** | [**PropertyTpaExtensions**](PropertyTpaExtensions.md) |  | [optional] 
**policies** | [**PropertyPolicies**](PropertyPolicies.md) |  | [optional] 
**area_info** | [**PropertyAreaInfo**](PropertyAreaInfo.md) |  | [optional] 
**affiliation_info** | [**PropertyAffiliationInfo**](PropertyAffiliationInfo.md) |  | [optional] 
**multimedia_descriptions** | [**Array&lt;PropertyMultimediaDescriptions&gt;**](PropertyMultimediaDescriptions.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject9.new(channel_id: 1,
                                 hotel_content: null,
                                 contact_info: null,
                                 hotel_info: null,
                                 facility_info: null,
                                 tpa_extensions: null,
                                 policies: null,
                                 area_info: null,
                                 affiliation_info: null,
                                 multimedia_descriptions: null)
```


