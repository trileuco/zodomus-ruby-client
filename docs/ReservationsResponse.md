# ZodomusClient::ReservationsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**reservations** | [**ReservationsResponseReservations**](ReservationsResponseReservations.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReservationsResponse.new(status: null,
                                 reservations: null)
```


