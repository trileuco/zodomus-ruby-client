# ZodomusClient::ReservationApi

All URIs are relative to *https://api.zodomus.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**reservations_cc_get**](ReservationApi.md#reservations_cc_get) | **GET** /reservations-cc | Get a reservation credit card info.
[**reservations_createtest_post**](ReservationApi.md#reservations_createtest_post) | **POST** /reservations-createtest | Create a test reservation for your test property
[**reservations_get**](ReservationApi.md#reservations_get) | **GET** /reservations | Get a reservation
[**reservations_queue_get**](ReservationApi.md#reservations_queue_get) | **GET** /reservations-queue | Get a list of reservations
[**reservations_summary_get**](ReservationApi.md#reservations_summary_get) | **GET** /reservations-summary | Get a list of future reservations



## reservations_cc_get

> ReservationsCCResponse reservations_cc_get(channel_id, property_id, reservation_id)

Get a reservation credit card info.

Get a reservation credit card info. You need a special key password to access this API. Check your backoffice for more informationa about your API Keys

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ReservationApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel
reservation_id = 'reservation_id_example' # String | Reservation id supplied by the channel in reservations-queue

begin
  #Get a reservation credit card info.
  result = api_instance.reservations_cc_get(channel_id, property_id, reservation_id)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ReservationApi->reservations_cc_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 
 **reservation_id** | **String**| Reservation id supplied by the channel in reservations-queue | 

### Return type

[**ReservationsCCResponse**](ReservationsCCResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## reservations_createtest_post

> reservations_createtest_post(inline_object3)

Create a test reservation for your test property

Creates a test reservation. status can be 'new', 'modified' or 'cancelled'. Optionally you can set a new reservation id

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ReservationApi.new
inline_object3 = ZodomusClient::InlineObject3.new # InlineObject3 | 

begin
  #Create a test reservation for your test property
  api_instance.reservations_createtest_post(inline_object3)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ReservationApi->reservations_createtest_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object3** | [**InlineObject3**](InlineObject3.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## reservations_get

> ReservationsResponse reservations_get(channel_id, property_id, reservation_id)

Get a reservation

Get a reservation complete information. The credit card information can be obtained in another API

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ReservationApi.new
channel_id = 56 # Integer | Channel id string
property_id = 'property_id_example' # String | Property id supplied by the channel
reservation_id = 'reservation_id_example' # String | Reservation id supplied by the channel in reservations-queue

begin
  #Get a reservation
  result = api_instance.reservations_get(channel_id, property_id, reservation_id)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ReservationApi->reservations_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id string | 
 **property_id** | **String**| Property id supplied by the channel | 
 **reservation_id** | **String**| Reservation id supplied by the channel in reservations-queue | 

### Return type

[**ReservationsResponse**](ReservationsResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## reservations_queue_get

> ReservationsQueueResponse reservations_queue_get(channel_id, property_id)

Get a list of reservations

Get a list of new, modified or cancelled reservations made by the channel

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ReservationApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel

begin
  #Get a list of reservations
  result = api_instance.reservations_queue_get(channel_id, property_id)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ReservationApi->reservations_queue_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 

### Return type

[**ReservationsQueueResponse**](ReservationsQueueResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## reservations_summary_get

> ReservationsSummaryResponse reservations_summary_get(channel_id, property_id)

Get a list of future reservations

Get a list of reservations scheduled for future dates that were created before your connection to this channel manager

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::ReservationApi.new
channel_id = 56 # Integer | Channel id
property_id = 'property_id_example' # String | Property id supplied by the channel

begin
  #Get a list of future reservations
  result = api_instance.reservations_summary_get(channel_id, property_id)
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling ReservationApi->reservations_summary_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id | 
 **property_id** | **String**| Property id supplied by the channel | 

### Return type

[**ReservationsSummaryResponse**](ReservationsSummaryResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

