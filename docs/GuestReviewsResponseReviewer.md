# ZodomusClient::GuestReviewsResponseReviewer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**country_code** | **String** |  | [optional] 
**is_genius** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::GuestReviewsResponseReviewer.new(name: null,
                                 country_code: null,
                                 is_genius: null)
```


