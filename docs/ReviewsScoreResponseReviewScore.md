# ZodomusClient::ReviewsScoreResponseReviewScore

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_count** | **String** |  | [optional] 
**score** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReviewsScoreResponseReviewScore.new(review_count: null,
                                 score: null)
```


