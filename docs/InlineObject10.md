# ZodomusClient::InlineObject10

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**property_id** | **String** |  | [optional] 
**property_status** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject10.new(channel_id: 1,
                                 property_id: 120120,
                                 property_status: Open,Close or Check)
```


