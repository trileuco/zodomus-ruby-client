# ZodomusClient::ReviewsScoreResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**reviews_scores** | [**Array&lt;ReviewsScoreResponseReviewsScores&gt;**](ReviewsScoreResponseReviewsScores.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReviewsScoreResponse.new(status: null,
                                 reviews_scores: null)
```


