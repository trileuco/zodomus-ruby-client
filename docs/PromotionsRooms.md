# ZodomusClient::PromotionsRooms

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roomid** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsRooms.new(roomid: null)
```


