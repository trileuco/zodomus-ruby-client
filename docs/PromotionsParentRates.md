# ZodomusClient::PromotionsParentRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent_rateid** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsParentRates.new(parent_rateid: null)
```


