# ZodomusClient::ReviewsScoreResponseReviewsScores

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**review_score** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 
**facilities** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 
**value** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 
**clean** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 
**location** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 
**staff** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 
**comfort** | [**ReviewsScoreResponseReviewScore**](ReviewsScoreResponseReviewScore.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::ReviewsScoreResponseReviewsScores.new(review_score: null,
                                 facilities: null,
                                 value: null,
                                 clean: null,
                                 location: null,
                                 staff: null,
                                 comfort: null)
```


