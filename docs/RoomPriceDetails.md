# ZodomusClient::RoomPriceDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**total** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::RoomPriceDetails.new(type: Guest or Hotel : roomPriceDetails are optional information sent by the channel,
                                 total: 260.00)
```


