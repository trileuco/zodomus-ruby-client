# ZodomusClient::PromotionsResponseStatsTotalRevenueAttributes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** |  | [optional] 
**currency** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseStatsTotalRevenueAttributes.new(value: null,
                                 currency: null)
```


