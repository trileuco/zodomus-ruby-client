# ZodomusClient::AccountAndInfoApi

All URIs are relative to *https://api.zodomus.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**account_get**](AccountAndInfoApi.md#account_get) | **GET** /account | Get account information
[**channels_get**](AccountAndInfoApi.md#channels_get) | **GET** /channels | Get a list of current active channels
[**currencies_get**](AccountAndInfoApi.md#currencies_get) | **GET** /currencies | Get a list of currencies
[**price_model_get**](AccountAndInfoApi.md#price_model_get) | **GET** /price-model | Get a list of price models



## account_get

> AccountResponse account_get

Get account information

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::AccountAndInfoApi.new

begin
  #Get account information
  result = api_instance.account_get
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling AccountAndInfoApi->account_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**AccountResponse**](AccountResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## channels_get

> ChannelResponse channels_get

Get a list of current active channels

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::AccountAndInfoApi.new

begin
  #Get a list of current active channels
  result = api_instance.channels_get
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling AccountAndInfoApi->channels_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ChannelResponse**](ChannelResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## currencies_get

> CurrenciesResponse currencies_get

Get a list of currencies

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::AccountAndInfoApi.new

begin
  #Get a list of currencies
  result = api_instance.currencies_get
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling AccountAndInfoApi->currencies_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**CurrenciesResponse**](CurrenciesResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## price_model_get

> PricemodelResponse price_model_get

Get a list of price models

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::AccountAndInfoApi.new

begin
  #Get a list of price models
  result = api_instance.price_model_get
  p result
rescue ZodomusClient::ApiError => e
  puts "Exception when calling AccountAndInfoApi->price_model_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PricemodelResponse**](PricemodelResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

