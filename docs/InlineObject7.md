# ZodomusClient::InlineObject7

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | 
**property_id** | **String** |  | 
**rooms** | [**Array&lt;RoomsActivationRooms&gt;**](RoomsActivationRooms.md) |  | 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject7.new(channel_id: 1,
                                 property_id: 999999,
                                 rooms: null)
```


