# ZodomusClient::PromotionsStayDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **String** |  | [optional] 
**_end** | **String** |  | [optional] 
**active_weekdays** | [**Array&lt;PromotionsStayDateActiveWeekdays&gt;**](PromotionsStayDateActiveWeekdays.md) |  | [optional] 
**excluded_dates** | [**Array&lt;PromotionsStayDateExcludedDates&gt;**](PromotionsStayDateExcludedDates.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsStayDate.new(start: null,
                                 _end: null,
                                 active_weekdays: null,
                                 excluded_dates: null)
```


