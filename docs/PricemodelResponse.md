# ZodomusClient::PricemodelResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**channels** | [**Array&lt;Channel&gt;**](Channel.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PricemodelResponse.new(status: null,
                                 channels: null)
```


