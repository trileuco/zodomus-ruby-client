# ZodomusClient::PromotionsResponseLastMinute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | [**PromotionsLastMinute**](PromotionsLastMinute.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseLastMinute.new(attributes: null)
```


