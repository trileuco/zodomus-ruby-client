# ZodomusClient::PromotionsResponseStayDateActiveWeekdays

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_weekday** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseStayDateActiveWeekdays.new(active_weekday: null)
```


