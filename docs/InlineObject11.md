# ZodomusClient::InlineObject11

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**status** | **String** |  | [optional] 
**name** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject11.new(channel_id: 1,
                                 status: New,
                                 name: Test Hotel)
```


