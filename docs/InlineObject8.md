# ZodomusClient::InlineObject8

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**property_id** | **String** |  | [optional] 
**rooms** | [**Array&lt;RoomsCancellationRooms&gt;**](RoomsCancellationRooms.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject8.new(channel_id: 1,
                                 property_id: 999999,
                                 rooms: null)
```


