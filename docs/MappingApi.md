# ZodomusClient::MappingApi

All URIs are relative to *https://api.zodomus.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**property_activation_post**](MappingApi.md#property_activation_post) | **POST** /property-activation | Activate a channel property with Zodomus
[**property_cancellation_post**](MappingApi.md#property_cancellation_post) | **POST** /property-cancellation | Cancel a channel property with Zodomus
[**property_check_post**](MappingApi.md#property_check_post) | **POST** /property-check | Check a property with Zodomus
[**rooms_activation_post**](MappingApi.md#rooms_activation_post) | **POST** /rooms-activation | Activate the property rooms and rates and map them with Zodomus
[**rooms_cancellation_post**](MappingApi.md#rooms_cancellation_post) | **POST** /rooms-cancellation | Cancel property rooms associated with Zodomus



## property_activation_post

> property_activation_post(inline_object4)

Activate a channel property with Zodomus

Activate a propertyId on the channelId selected.<br><br> priceModelId should be<br><br> 1 = Maximum / single (Booking only, recommended price by Booking)<br> 2 = Derived prices (Booking only)<br> 3 = Occupancy (Expedia only, recommended by Expedia)<br> 4 = Per day / Length of stay (Booking and Expedia)<br>

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::MappingApi.new
inline_object4 = ZodomusClient::InlineObject4.new # InlineObject4 | 

begin
  #Activate a channel property with Zodomus
  api_instance.property_activation_post(inline_object4)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling MappingApi->property_activation_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object4** | [**InlineObject4**](InlineObject4.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## property_cancellation_post

> property_cancellation_post(inline_object5)

Cancel a channel property with Zodomus

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::MappingApi.new
inline_object5 = ZodomusClient::InlineObject5.new # InlineObject5 | 

begin
  #Cancel a channel property with Zodomus
  api_instance.property_cancellation_post(inline_object5)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling MappingApi->property_cancellation_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object5** | [**InlineObject5**](InlineObject5.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## property_check_post

> property_check_post(inline_object6)

Check a property with Zodomus

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::MappingApi.new
inline_object6 = ZodomusClient::InlineObject6.new # InlineObject6 | 

begin
  #Check a property with Zodomus
  api_instance.property_check_post(inline_object6)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling MappingApi->property_check_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object6** | [**InlineObject6**](InlineObject6.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## rooms_activation_post

> rooms_activation_post(inline_object7)

Activate the property rooms and rates and map them with Zodomus

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::MappingApi.new
inline_object7 = ZodomusClient::InlineObject7.new # InlineObject7 | 

begin
  #Activate the property rooms and rates and map them with Zodomus
  api_instance.rooms_activation_post(inline_object7)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling MappingApi->rooms_activation_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object7** | [**InlineObject7**](InlineObject7.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## rooms_cancellation_post

> rooms_cancellation_post(inline_object8)

Cancel property rooms associated with Zodomus

### Example

```ruby
# load the gem
require 'zodomus_client'
# setup authorization
ZodomusClient.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = ZodomusClient::MappingApi.new
inline_object8 = ZodomusClient::InlineObject8.new # InlineObject8 | 

begin
  #Cancel property rooms associated with Zodomus
  api_instance.rooms_cancellation_post(inline_object8)
rescue ZodomusClient::ApiError => e
  puts "Exception when calling MappingApi->rooms_cancellation_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object8** | [**InlineObject8**](InlineObject8.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

