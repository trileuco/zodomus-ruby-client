# ZodomusClient::PromotionsBookDate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **String** |  | [optional] 
**_end** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsBookDate.new(start: null,
                                 _end: null)
```


