# ZodomusClient::PromotionsResponseEarlyBokerAttributes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponseEarlyBokerAttributes.new(value: null)
```


