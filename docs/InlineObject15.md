# ZodomusClient::InlineObject15

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **Integer** |  | [optional] 
**property_id** | **String** |  | [optional] 
**action** | **String** |  | [optional] 
**opportunity_id** | **String** |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::InlineObject15.new(channel_id: 1,
                                 property_id: 999999,
                                 action: ENABLE or DISMISS,
                                 opportunity_id: AD5252522)
```


