# ZodomusClient::PromotionsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**Status**](Status.md) |  | [optional] 
**promotions** | [**Array&lt;PromotionsResponsePromotions&gt;**](PromotionsResponsePromotions.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PromotionsResponse.new(status: null,
                                 promotions: null)
```


