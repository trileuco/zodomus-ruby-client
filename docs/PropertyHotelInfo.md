# ZodomusClient::PropertyHotelInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guest_room_quantity** | **String** |  | [optional] 
**property_type** | **String** |  | [optional] 
**services** | [**Array&lt;PropertyHotelInfoServices&gt;**](PropertyHotelInfoServices.md) |  | [optional] 
**accepted_payments** | [**Array&lt;PropertyHotelInfoServices&gt;**](PropertyHotelInfoServices.md) |  | [optional] 
**transportations** | [**Array&lt;PropertyHotelInfoTransportations&gt;**](PropertyHotelInfoTransportations.md) |  | [optional] 

## Code Sample

```ruby
require 'ZodomusClient'

instance = ZodomusClient::PropertyHotelInfo.new(guest_room_quantity: 1,
                                 property_type: 1,
                                 services: null,
                                 accepted_payments: null,
                                 transportations: null)
```


